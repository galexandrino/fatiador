# Fatiador

[![build status](https://gitlab.com/serpro/fatiador/badges/master/build.svg)](https://gitlab.com/serpro/fatiador/commits/master)

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/3628342d9a4f4a10b3aceac77ff46ecb)](https://www.codacy.com/app/leonardofl87/fatiador?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=serpro/fatiador/&amp;utm_campaign=Badge_Grade)

[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/3628342d9a4f4a10b3aceac77ff46ecb)](https://www.codacy.com/app/leonardofl87/fatiador?utm_source=gitlab.com&utm_medium=referral&utm_content=serpro/fatiador/&utm_campaign=Badge_Coverage)

## What is

Fatiador converts flat strings to Java objects and vice-versa. 

And what is a flat string? It's a serialized data representation meant to data exchange between systems, like JSON or XML. It's very used in Mainframe systems built with Cobol or Natural languages.

In a flat string, each data piece has a fixed length and the values are just concatenated one after another. For example, a flat string representing a `person` with a `name`, a `birthday` and a `country` could be `'Richard Stallman    19530316USA       '`, in which the name must have a length of 20, the birthday must have a length of 8 and the country must have a length of 10.

When writing Java systems that communicate with Mainframe systems, it's common to have to translate flat strings into Java objects and vice-versa. So Fatiador is here to help you in this task.

## Why to use it

Before writing this Fatiador, we have searched for alternatives. But most of them were not so easy to setup, since they were often embedded into larger frameworks. 

Moreover, we have a very special requirement not handled by the found alternatives: flat strings with lists. Consider that a flat string of a `person` is now composed by the `name of the person` (length of 15) and by a list of the `names of the dogs` of this person (each dog name has length 5). Between the person name and the dog names it's necessary to declare the `quantity of dogs` in the list, and this quantity also have a fixed length, let's say 3. So, an example of such flat string is: `'Roger Dearly   002PongoPerdy'`. 

So, in short, Fatiador is very easy to set up and to use. And it can help you in slicing flat strings such as `'Roger Dearly   002PongoPerdy'`.

## How to use

First of all, add the following in your pom.xml:

```xml
	<dependency>
		<groupId>br.gov.serpro.fatiador</groupId>
		<artifactId>fatiador</artifactId>
		<version>1.18.0</version>
	</dependency>	
```

To be sure about the last available version, check https://mvnrepository.com/artifact/br.gov.serpro.fatiador/fatiador.

Now, let's say we have a `Book` class:


```java
    public class Book {

	    private String title;
	    private String author;
	    private String isbn;

        // getters and setters
        // equals and hashCode
        // toString
    }
```

Let's first define the flat structure of a book:

```java
    FlatStructure bookStructure = new FlatStructure();
    bookStructure.addSimpleAlphaField("title", 15);
    bookStructure.addSimpleAlphaField("author", 15);
    bookStructure.addSimpleNumericField("isbn", 13);
```

Now let's convert a flat string into a book object:

```java
    String flatInput = "War and Peace  Leo Tolstoy    ";
    FlatParser<Book> parser = new FlatParser<>(bookStructure, Book.class);
    Book book = parser.parse(flatInput);
```

Finally, let's convert a book object into a flat string:

```java
    Book book = new Book("War and Peace", "Leo Tolstoy", "123456");
    FlatWriter<Book> writer = new FlatWriter<>(bookStructure, Book.class);
    String flat = writer.write(book);
```

See the [parser tests](https://gitlab.com/serpro/fatiador/blob/master/src/test/java/fatiador/FlatParserTest.java) to more examples on how to slice flat strings.

See the [writer tests](https://gitlab.com/serpro/fatiador/blob/master/src/test/java/fatiador/FlatWriterTest.java) to more examples on how to generate flat strings.

You can also convert a flat String to a Java map, so you don't need to define a Book class:

```java
    MapFlatParser parser = new MapFlatParser(bookStructure);
    Map<String, String> book = parser.parse(flatInput);
```



