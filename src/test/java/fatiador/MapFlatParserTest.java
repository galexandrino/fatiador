package fatiador;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

/**
 * Parse flat strings to Java maps.
 */
import org.junit.Test;

import fatiador.parser.MapFlatParser;

public class MapFlatParserTest {

    @Test
    public void shouldParseSimpleFields() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);

        String flatInput = "12345678901Leonardo  ";

        MapFlatParser parser = new MapFlatParser(personStructure);
        Map<String, String> person = parser.parse(flatInput);
        
        Map<String, String> expectedPerson = new HashMap<>();
        expectedPerson.put("identificationNumber", "12345678901");
        expectedPerson.put("name", "Leonardo");
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseIntMultipleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);

        String flatInput = "12345678901Leonardo  03121314";

        MapFlatParser parser = new MapFlatParser(personStructure);
        Map<String, String> person = parser.parse(flatInput);

        Map<String, String> expectedPerson = new HashMap<>();
        expectedPerson.put("identificationNumber", "12345678901");
        expectedPerson.put("name", "Leonardo");
        expectedPerson.put("scores", "12, 13, 14");
        assertEquals(expectedPerson, person);
    }
    
    @Test
    public void shouldParseAllFields() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);
        personStructure.addSimpleIntegerField("age", 2);
        personStructure.addSimpleDecimalField("weight", 5, 2);

        String flatInput = "12345678901Leonardo  031213143rex  dino milu 2907599";

        MapFlatParser parser = new MapFlatParser(personStructure);
        Map<String, String> person = parser.parse(flatInput);

        Map<String, String> expectedPerson = new HashMap<>();
        expectedPerson.put("identificationNumber", "12345678901");
        expectedPerson.put("name", "Leonardo");
        expectedPerson.put("scores", "12, 13, 14");
        expectedPerson.put("dogNames", "rex, dino, milu");
        expectedPerson.put("age", "29");
        expectedPerson.put("weight", "75.99");
        assertEquals(expectedPerson, person);
    }
    
    @Test
    public void shouldParseMultipleStructuredField() {

        FlatStructure dogStructure = new FlatStructure();
        dogStructure.addSimpleAlphaField("name", 5);
        dogStructure.addSimpleAlphaField("breed", 10);

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("dogs", 2);
        personStructure.addMultipleStructuredField("dogs", dogStructure, Dog.class);

        String flatInput = "12345678901Leonardo  02Rex  Husky     Totó Poodle    ";

        MapFlatParser parser = new MapFlatParser(personStructure);
        Map<String, String> person = parser.parse(flatInput);

        Map<String, String> expectedPerson = new HashMap<>();
        expectedPerson.put("identificationNumber", "12345678901");
        expectedPerson.put("name", "Leonardo");
        expectedPerson.put("dogs", "Dog [name=Rex, breed=Husky], Dog [name=Totó, breed=Poodle]");

        assertEquals(expectedPerson, person);
    }
    
    @Test
    public void shouldParseSimpleDateFieldToEmptyStringWhenFlatDateIsZeros() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDateField("birthday", "yyyyMMdd");

        String dateZeros = "00000000";

        String flatInput = "12345678901Leonardo  " + dateZeros;

        MapFlatParser parser = new MapFlatParser(personStructure);
        Map<String, String> person = parser.parse(flatInput);

        Map<String, String> expectedPerson = new HashMap<>();
        expectedPerson.put("identificationNumber", "12345678901");
        expectedPerson.put("name", "Leonardo");
        expectedPerson.put("birthday", "");

        assertEquals(expectedPerson, person);
    }

}
