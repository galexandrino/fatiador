package fatiador;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Pet {
    
    public String name;
    public String specie;
    
    public Pet() {
        
    }
    
    public Pet(String name, String specie) {
        this.name = name;
        this.specie = specie;
    }
    
    @Override
    public boolean equals(final Object other) {
        if (!(other instanceof Pet)) {
            return false;
        }
        Pet castOther = (Pet) other;
        return new EqualsBuilder().append(name, castOther.name).append(specie, castOther.specie).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(name).append(specie).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("specie", specie).toString();
    }
    
}
