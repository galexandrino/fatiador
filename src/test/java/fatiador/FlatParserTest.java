package fatiador;

import static fatiador.PersonBuilder.aPerson;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Ignore;
import org.junit.Test;

import fatiador.pojo.FlatFieldOptions;

public class FlatParserTest {
    @Test
    public void shouldThrowExceptionWhenNumericFieldNotParsable() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleNumericField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);

        String flatInput = "123A       Leonardo  ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains("identificationNumber"));
            assertTrue(e.getMessage().contains("123A"));
        }

    }

    @Test
    public void shouldParseMultipleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);

        String flatInput = "12345678901Leonardo  3rex  dino milu ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo");
        expectedPerson.setDogs("rex", "dino", "milu");
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseIntMultipleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);

        String flatInput = "12345678901Leonardo  03121314";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo");
        expectedPerson.setScores(12, 13, 14);
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseAllFields() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);
        personStructure.addSimpleIntegerField("age", 2);
        personStructure.addSimpleDecimalField("weight", 5, 2);

        String flatInput = "12345678901Leonardo  031213143rex  dino milu 2907599";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);


        Person expectedPerson = aPerson("12345678901", "Leonardo")
                                .withAge(29).withWeight(75.99).build();
        expectedPerson.setScores(12, 13, 14);
        expectedPerson.setDogs("rex", "dino", "milu");
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseListasVazias() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("scores", 2);
        personStructure.addMultipleIntegerField("scores", 2);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);

        String flatInput = "12345678901Leonardo  000";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = aPerson("12345678901", "Leonardo").build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseMultipleStructuredField() {
        FlatStructure personStructure = createPersonStructure();
        String flatInput = "12345678901Leonardo  02Rex  Husky     Totó Poodle    ";
        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        Person expectedPerson = createExpectedPerson();
        assertEquals(expectedPerson, person);
    }

    private FlatStructure createPersonStructure() {
        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("dogs", 2);
        FlatStructure dogStructure = createDogStructure();
        personStructure.addMultipleStructuredField("dogs", dogStructure, Dog.class);
        return personStructure;
    }

    private FlatStructure createDogStructure() {
        FlatStructure dogStructure = new FlatStructure();
        dogStructure.addSimpleAlphaField("name", 5);
        dogStructure.addSimpleAlphaField("breed", 10);
        return dogStructure;
    }

    private Person createExpectedPerson() {
        Person expectedPerson = new Person("12345678901", "Leonardo");
        expectedPerson.dogs.add(createExpectedDog("Rex", "Husky"));
        expectedPerson.dogs.add(createExpectedDog("Totó", "Poodle"));
        return expectedPerson;
    }

    private Dog createExpectedDog(String name, String breed) {
        Dog expectedDog = new Dog();
        expectedDog.name = name;
        expectedDog.breed = breed;
        return expectedDog;
    }


    @Test
    public void shouldParseTwoMultipleStructuredFields() {
        FlatStructure songStructure = createSongStructure();
        FlatStructure dogStructure = createDogStructure();
        FlatStructure personStructure = createPersonSongStructure();

        String flatInput = "Leonardo  02Rex  Husky     Totó Poodle    02Time           Pink FloydNothing to say Angra     ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = createExpectedPersonSong();

        assertEquals(expectedPerson, person);
    }

    private FlatStructure createSongStructure() {
        FlatStructure songStructure = new FlatStructure();
        songStructure.addSimpleAlphaField("name", 15);
        songStructure.addSimpleAlphaField("artist", 10);
        return songStructure;
    }

    private FlatStructure createPersonSongStructure() {
        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("dogs", 2);
        personStructure.addMultipleStructuredField("dogs", createDogStructure(), Dog.class);
        personStructure.addLengthField("favoriteSongs", 2);
        personStructure.addMultipleStructuredField("favoriteSongs", createSongStructure(), Song.class);
        return personStructure;
    }

    private Person createExpectedPersonSong() {
        Person expectedPerson = new Person("Leonardo");
        expectedPerson.dogs.add(new Dog("Rex", "Husky"));
        expectedPerson.dogs.add(new Dog("Totó", "Poodle"));
        expectedPerson.favoriteSongs.add(new Song("Time", "Pink Floyd"));
        expectedPerson.favoriteSongs.add(new Song("Nothing to say", "Angra"));
        return expectedPerson;
    }


    @Test
    public void shouldParseMultipleStructuredFieldForListFixedSize() {
        FlatStructure personStructure = createPersonStructureWithFixedSizeList();

        String flatInput = "1234567890102Rex  Husky     Totó Poodle                                  Leonardo  ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = createExpectedPersonWithFixedSizeList();

        assertEquals(expectedPerson, person);
    }

    private FlatStructure createPersonStructureWithFixedSizeList() {
        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addLengthField("dogs", 2);
        int listFixedSize = 4;
        FlatFieldOptions options = new FlatFieldOptions("dogs", listFixedSize, null);
        personStructure.addMultipleStructuredField(options, createDogStructure(), Dog.class);
        personStructure.addSimpleAlphaField("name", 10);
        return personStructure;
    }

    private Dog createDog(String name, String breed) {
        Dog dog = new Dog();
        dog.name = name;
        dog.breed = breed;
        return dog;
    }

    private Person createExpectedPersonWithFixedSizeList() {
        Person expectedPerson = new Person("12345678901", "Leonardo");
        expectedPerson.dogs.add(createDog("Rex", "Husky"));
        expectedPerson.dogs.add(createDog("Totó", "Poodle"));
        return expectedPerson;
    }

    @Test
    public void shouldParseWritingOnPrivateFields() {

        FlatStructure bookStructure = new FlatStructure();
        bookStructure.addSimpleAlphaField("title", 15);
        bookStructure.addSimpleAlphaField("author", 15);

        String flatInput = "War and Peace  Leo Tolstoy    ";

        FlatParser<Book> parser = new FlatParser<>(bookStructure, Book.class);
        Book book = parser.parse(flatInput);

        Book expectedBook = new Book("War and Peace", "Leo Tolstoy");
        assertEquals(expectedBook, book);
    }

    @Test
    public void shouldParseBooleanFieldToTrue() {

        FlatStructure personStructure = new FlatStructure();
        String flatTrueValue = "1";
        String flatFalseValue = "0";
        personStructure.addBooleanField("isAdult", flatTrueValue, flatFalseValue);
        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);

        String flatInput = "1";
        Person person = parser.parse(flatInput);

        assertTrue(person.isAdult);
    }

    @Test
    public void shouldParseBooleanFieldToFalse() {

        FlatStructure personStructure = new FlatStructure();
        String flatTrueValue = "1";
        String flatFalseValue = "0";
        personStructure.addBooleanField("isAdult", flatTrueValue, flatFalseValue);
        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);

        String flatInput = "0";
        Person person = parser.parse(flatInput);

        assertFalse(person.isAdult);
    }

    @Test
    public void shouldNotParseInvalidBooleanField() {

        FlatStructure personStructure = new FlatStructure();
        String flatTrueValue = "1";
        String flatFalseValue = "0";
        personStructure.addBooleanField("isAdult", flatTrueValue, flatFalseValue);
        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);

        String flatInput = "7";

        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains("isAdult"));
            assertTrue(e.getMessage().contains("7"));
            assertTrue(e.getMessage().contains("0"));
            assertTrue(e.getMessage().contains("1"));
        }

    }

    @Test
    public void shouldShowPartialParseWhenFail() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 15);
        personStructure.addSimpleAlphaField("age", 2);

        String invalidAge = "DU";
        String flatInput = "12345678901Homer Simpson  " + invalidAge;

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);

        try {
            parser.parse(flatInput);
        } catch (Exception e) {
            assertThat(e.getMessage(), containsString("identificationNumber"));
            assertThat(e.getMessage(), containsString("12345678901"));
            assertThat(e.getMessage(), containsString("name"));
            assertThat(e.getMessage(), containsString("Homer Simpson"));
        }

    }

    @Test
    @Ignore("Not supported; #23")
    public void shouldParseWithInheritance() {

        FlatStructure catStructure = new FlatStructure();
        catStructure.addSimpleAlphaField("name", 10);
        catStructure.addSimpleAlphaField("specie", 10);
        catStructure.addSimpleAlphaField("color", 10);

        String flatInput = "Felix     cat       black     ";

        FlatParser<Cat> parser = new FlatParser<>(catStructure, Cat.class);
        Cat cat = parser.parse(flatInput);

        Cat expectedCat = new Cat("Felix", "cat", "black");
        assertEquals(expectedCat, cat);
    }

    @Test
    public void shouldTrimFinalSpaces() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);

        String flatInput = "12345678901Leonardo  ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo");
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldNotTrimInitialSpaces() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);

        String flatInput = "12345678901  Leonardo";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "  Leonardo");
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseEmptyList() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("dogNames", 1);
        personStructure.addMultipleAlphaField("dogNames", 5);

        String flatInput = "12345678901Leonardo  0";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo");
        assertEquals(expectedPerson, person);
        assertTrue(person.dogs.isEmpty()); // just for clarity
    }

    @Test
    public void shouldThrowNiceErrorWhenWritingToNullCollection() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addLengthField("creditCards", 2);
        personStructure.addMultipleAlphaField("creditCards", 10);

        String flatInput = "12345678901Leonardo  011234567890";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);

        try {
            parser.parse(flatInput);
            fail("We expect an error");
        } catch (Exception e) {
            assertTrue(e.getMessage().contains("Collection 'creditCards' is null"));
        }
    }

}
