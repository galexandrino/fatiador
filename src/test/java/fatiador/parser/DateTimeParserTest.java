package fatiador.parser;

import fatiador.FlatType;
import fatiador.ParserArgumentException;
import fatiador.SimpleField;
import fatiador.parser.DateParser;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class DateTimeParserTest {

    DateParser parser = new DateParser();
    SimpleField field = new SimpleField("date", "yyyyMMddHHmmssS", FlatType.DATE_TIME);

    @Test
    public void shouldParseWithCorrectFormat() {
        try {
            parser.parse("201903140923059", field);
        } catch (ParserArgumentException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldThrowExceptionWithInvalidDay() {
        try {
            parser.parse("201903320445327", field);
            fail();
        } catch (ParserArgumentException e) {
            assertTrue(e.getMessage().contains("date"));
            assertTrue(e.getMessage().contains("32"));
        }
    }

    @Test
    public void shouldThrowExceptionWithSpacesAtTheBegin() {
        try {
            parser.parse("   201903140445327", field);
            fail();
        } catch (ParserArgumentException e) {
            assertTrue(e.getMessage().contains("date"));
            assertTrue(e.getMessage().contains("   2019"));
        }
    }

    @Test
    public void shouldThrowExceptionWithAlphaAtTheMiddle() {
        try {
            parser.parse("2019MA140445327", field);
            fail();
        } catch (ParserArgumentException e) {
            assertTrue(e.getMessage().contains("date"));
            assertTrue(e.getMessage().contains("MA"));
        }
    }

    @Test
    public void shouldThrowExceptionWithFormatConflict() {
        try {
            parser.parse("140320190730478", field);
            fail();
        } catch (ParserArgumentException e) {
            assertTrue(e.getMessage().contains("date"));
            assertTrue(e.getMessage().contains("14032019"));
        }
    }

}
