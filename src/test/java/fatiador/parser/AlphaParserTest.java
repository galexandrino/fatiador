package fatiador.parser;

import static org.junit.Assert.fail;

import org.junit.Test;

import fatiador.FlatType;
import fatiador.ParserArgumentException;
import fatiador.SimpleField;
import fatiador.parser.AlphaParser;

public class AlphaParserTest {

    AlphaParser parser = new AlphaParser();
    SimpleField field = new SimpleField("text", 100, FlatType.ALPHA);

    @Test
    public void shouldParseWithUpperCaseOnly() {
        try {
            parser.parse("ABC", field);
        } catch (ParserArgumentException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldParseWithLowerCaseOnly() {
        try {
            parser.parse("xyz", field);
        } catch (ParserArgumentException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldParseWithAlphaAndNumber() {
        try {
            parser.parse("ABC123", field);
        } catch (ParserArgumentException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldParseWithSpacePadAtTheLeft() {
        try {
            parser.parse("          ABCDEFGHIJ123456789012345678901234567890", field);
        } catch (ParserArgumentException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldParseWithSpacePadAtTheRight() {
        try {
            parser.parse("ABCDEFGEHI123456789012345678901234567890          ", field);
        } catch (ParserArgumentException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldParseWithPunctiation() {
        try {
            parser.parse("A, B, C; X, Y, Z!", field);
        } catch (ParserArgumentException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldParseWithSimbols() {
        try {
            parser.parse("a = b + c - d", field);
        } catch (ParserArgumentException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldParseWithSpecialChars() {
        try {
            parser.parse("Açaí? Não, Maniçoba! R$ 20,00. Envia nota? SMS ou e-mail? daniel@fatiador.serpro.gov.br, por favor.", field);
        } catch (ParserArgumentException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldParseWithSmallRealText() {
        try {
            parser.parse("Minha terra tem palmeiras, Onde canta o Sabiá", field);
        } catch (ParserArgumentException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void shouldParseWithControlChars() {
        try {
            parser.parse("\tMinha terra tem palmeiras,\n\tOnde canta o Sabiá", field);
        } catch (ParserArgumentException e) {
            fail(e.getMessage());
        }
    }

}