package fatiador.parser;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fatiador.ParserArgumentException;
import fatiador.SimpleField;
import fatiador.pojo.FlatFieldOptions;

public class BooleanParserTest {

    BooleanParser parser = new BooleanParser();
    FlatFieldOptions options = new FlatFieldOptions("fieldName", 1, null);
    SimpleField field = new SimpleField(options, "1", "0");

    @Test
    public void parse_true() throws Exception {
        assertTrue(parser.parse("1", field));
    }

    @Test
    public void parse_false() throws Exception {
        assertFalse(parser.parse("0", field));
    }

    @Test(expected = ParserArgumentException.class)
    public void not_parse_invalid_value() throws Exception  {
        parser.parse("X", field);
    }

}
