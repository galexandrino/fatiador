package fatiador.writer;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fatiador.SimpleField;
import fatiador.pojo.FlatFieldOptions;

public class BooleanWriterTest {

    BooleanWriter writer = new BooleanWriter();
    FlatFieldOptions options = new FlatFieldOptions("fieldName", 1, null);
    SimpleField booleanField = new SimpleField(options, "1", "0");

    @Test
    public void write_true() {
        Boolean value = true;
        String flatValue = writer.write(value, booleanField);
        assertEquals("1", flatValue);
    }

    @Test
    public void write_false() {
        Boolean value = false;
        String flatValue = writer.write(value, booleanField);
        assertEquals("0", flatValue);
    }

}
