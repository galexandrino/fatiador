package fatiador;

import static org.junit.Assert.*;

import org.junit.Test;

import fatiador.pojo.FlatFieldOptions;

public class FlatStructureTest {

    @Test
    public void shouldReturnStructureLength() {

        FlatStructure dogStructure = new FlatStructure();
        dogStructure.addSimpleAlphaField("name", 5);
        dogStructure.addSimpleAlphaField("breed", 10);
        dogStructure.addSimpleIntegerField("age", 2);
        dogStructure.addSimpleDecimalField("weight", 4, 2);

        assertEquals(21, dogStructure.length());
    }

    @Test(expected = IllegalStateException.class)
    public void shouldNotCalculateLengthOfStructureWithMultipleField() {

        FlatStructure dogStructure = new FlatStructure();
        dogStructure.addSimpleAlphaField("name", 5);
        dogStructure.addSimpleAlphaField("breed", 10);
        dogStructure.addSimpleIntegerField("age", 2);
        dogStructure.addSimpleDecimalField("weight", 4, 2);
        dogStructure.addLengthField("fleasNames", 2);
        dogStructure.addMultipleAlphaField("fleasNames", 10);

        dogStructure.length();
    }

    @Test
    public void shoulCalculateLengthOfStructureWithMultipleFieldWithFixedSize() {

        FlatStructure dogStructure = new FlatStructure();
        dogStructure.addSimpleAlphaField("name", 5);
        dogStructure.addSimpleAlphaField("breed", 10);
        dogStructure.addSimpleIntegerField("age", 2);
        dogStructure.addSimpleDecimalField("weight", 4, 2);
        dogStructure.addLengthField("fleasNames", 2);
        FlatFieldOptions options = new FlatFieldOptions("fleasNames", 10, 5);
        dogStructure.addMultipleAlphaField(options);

        assertEquals(73, dogStructure.length());
    }
}
