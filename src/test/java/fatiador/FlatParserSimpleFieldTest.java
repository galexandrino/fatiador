package fatiador;

import static fatiador.PersonBuilder.aPerson;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.junit.Ignore;
import org.junit.Test;

public class FlatParserSimpleFieldTest {
    @Test
    public void shouldParseSimpleFields() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);

        String flatInput = "12345678901Leonardo  ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo");
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseSimpleFieldsWithFiller() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.setFillerField("identificationNumber");

        String flatInput = "12345678901Leonardo  ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person(null, "Leonardo");
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseSimpleFieldsWithNumericIdNumber() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleNumericField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);

        String flatInput = "123        Leonardo  ";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("123", "Leonardo");
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseIntegerSimpleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleIntegerField("age", 2);

        String flatInput = "12345678901Leonardo  29";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = aPerson("12345678901", "Leonardo").withAge(29).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseDecimalSimpleField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDecimalField("weight", 5, 2);

        String flatInput = "12345678901Leonardo  07599";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);

        Person expectedPerson = new Person("12345678901", "Leonardo", 75.99);
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseSimpleDateField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDateField("birthday", "yyyyMMdd");

        String flatInput = "12345678901Leonardo  19870607";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        LocalDate birthday = LocalDate.of(1987, 06, 07);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthday(birthday).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldNotParseSimpleDateFieldWithInvalidDate() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDateField("birthday", "yyyyMMdd");

        String invalidDate = "19871340";
        String flatInput = "12345678901Leonardo  " + invalidDate;

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains("birthday"));
            assertTrue(e.getMessage().contains(invalidDate));
        }
    }

    @Test
    public void shouldNotParseSimpleDateFieldWithInvalidDateFormat() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        String invalidDateFormat = "YYYYmmDD";
        personStructure.addSimpleDateField("birthday", invalidDateFormat);

        String flatInput = "12345678901Leonardo  19870607";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        try {
            parser.parse(flatInput);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains("birthday"));
            assertTrue(e.getMessage().contains(invalidDateFormat));
        }
    }

    @Test
    public void shouldParseSimpleTimeField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleTimeField("birthTime", "HHmmss");

        String flatInput = "12345678901Leonardo  160834";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        LocalTime birthTime = LocalTime.of(16, 8, 34);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthTime(birthTime).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseSimpleDateTimeField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDateTimeField("birthDateTime", "yyyyMMddHHmmss");

        String flatInput = "12345678901Leonardo  19870607160834";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        LocalDateTime birthDateTime = LocalDateTime.of(1987, 06, 07, 16, 8, 34);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthDateTime(birthDateTime).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseSimpleDateTimeWithPatternSField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDateTimeField("birthDateTime", "yyyyMMddHHmmssS");

        String flatInput = "12345678901Leonardo  198706071608347";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        LocalDateTime birthDateTime = LocalDateTime.of(1987, 06, 07, 16, 8, 34, 700 * 1000 * 1000);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthDateTime(birthDateTime).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseSimpleTimeWithPatternSField() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleTimeField("birthTime", "HHmmssS");

        String flatInput = "12345678901Leonardo  1608346";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        LocalTime birthTime = LocalTime.of(16, 8, 34, 600 * 1000 * 1000);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthTime(birthTime).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseSimpleDateFieldToNullWhenFlatDateIsBlank() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDateField("birthday", "yyyyMMdd");

        String dateBlank = "        ";

        String flatInput = "12345678901Leonardo  " + dateBlank;

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthday(null).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseSimpleDateFieldToNullWhenFlatDateIsZeros() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDateField("birthday", "yyyyMMdd");

        String dateZeros = "00000000";

        String flatInput = "12345678901Leonardo  " + dateZeros;

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthday(null).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseSimpleTimeFieldToNullWhenFlatTimeIsBlank() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleTimeField("birthTime", "HHmmss");

        String blankTime = "      ";

        String flatInput = "12345678901Leonardo  " + blankTime;

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthTime(null).build();
        assertEquals(expectedPerson, person);
    }

    @Test
    public void shouldParseSimpleDateTimeFieldWithZeros() {

        FlatStructure personStructure = new FlatStructure();
        personStructure.addSimpleAlphaField("identificationNumber", 11);
        personStructure.addSimpleAlphaField("name", 10);
        personStructure.addSimpleDateTimeField("birthDateTime", "yyyyMMddHHmmssS");

        String flatInput = "12345678901Leonardo  00000000000000000";

        FlatParser<Person> parser = new FlatParser<>(personStructure, Person.class);
        Person person = parser.parse(flatInput);
        Person expectedPerson = aPerson("12345678901", "Leonardo").withBirthDateTime(null).build();
        assertEquals(expectedPerson, person);
    }
}
