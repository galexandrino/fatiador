package fatiador;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Cat extends Pet {

    public String color;

    public Cat() {
        
    }

    public Cat(String name, String specie, String color) {
        super(name, specie);
        this.color = color;
    }

    @Override
    public boolean equals(final Object other) {
        if (!(other instanceof Cat)) {
            return false;
        }
        Cat castOther = (Cat) other;
        return new EqualsBuilder().appendSuper(super.equals(other)).append(color, castOther.color).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().appendSuper(super.hashCode()).append(color).toHashCode();
    }

    @Override
    public String toString() {
        return "Cat [color=" + color + ", name=" + name + ", specie=" + specie + "]";
    }
    
}
