package fatiador.parser;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import fatiador.FlatField;
import fatiador.ParserArgumentException;

public class BooleanParser implements FieldParser<Boolean> {

    private static final String ERROR_MESSAGE = "Field [%s] not parsable. Value [%s] not convertible to Boolean for True = [%s] and False = [%s].";

    @Override
    public Boolean parse(String rawValue, FlatField flatField) throws ParserArgumentException {
    	
    	if (StringUtils.isBlank(rawValue)) {
    		return false;
    	}
    	
        String flatTrue = flatField.getFlatTrue();
        String flatFalse = flatField.getFlatFalse();
        try {
            return BooleanUtils.toBoolean(rawValue, flatTrue, flatFalse);
        } catch (IllegalArgumentException e) {
            String msg = String.format(ERROR_MESSAGE, flatField.getName(), rawValue, flatTrue, flatFalse);
            throw new ParserArgumentException(msg, e);
        }
    }

}
