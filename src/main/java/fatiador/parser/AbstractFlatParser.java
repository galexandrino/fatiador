package fatiador.parser;

import java.util.HashMap;
import java.util.Map;

import com.google.common.base.Splitter;

import fatiador.FlatField;
import fatiador.FlatStructure;
import fatiador.LengthField;
import fatiador.MultipleField;
import fatiador.ParserArgumentException;
import fatiador.SimpleField;
import fatiador.pojo.FlatData;
import fatiador.pojo.ParserPointer;

public abstract class AbstractFlatParser<T> {
    private FlatStructure structure;
    private Map<String, Integer> quantityOfChunksOfMultipleFields = new HashMap<>();
    private FieldParserFactory fieldParserFactory = new FieldParserFactory();
    public AbstractFlatParser(FlatStructure structure) { this.structure = structure; }
    public T parse(String flatInput) {
        try {
            return innerParse(flatInput);
        } catch (ParserArgumentException e) {
            String msg = e.getMessage() + "\nFlat string = '" + flatInput + "'; partial debug: " + getBean().toString();
            throw new IllegalArgumentException(msg, e.getCause());
        } catch (Exception e) {
            String msg = "Flat string = '" + flatInput + "'; partial debug: " + getBean().toString();
            throw new IllegalStateException(msg, e);
        }
    }
    public abstract void initializeBean();
    public abstract T getBean();
    public abstract void setValue(String fieldName, Object value);
    public abstract void addInCollection(String fieldName, Object value) throws ParserArgumentException;
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private T innerParse(String flatInput) throws Exception  {
        initializeBean();
        int pos = 0;
        for (FlatField flatField : structure.getFields()) {
            String name = flatField.getName();
            int size = flatField.getSize();
            if (flatField.getFiller()) {
				setFillerField(name);
                pos += size;
                continue;
            } else if (flatField instanceof SimpleField) {
                handleSimpleField(new FlatData(flatInput, flatField), name, new ParserPointer(pos, size));
                pos += size;
            } else if (flatField instanceof LengthField) {
                handleLengthField(new FlatData(flatInput, flatField), name, new ParserPointer(pos, size));
                pos += size;
            } else if (flatField instanceof MultipleField)
                pos = handleMultipleField(new FlatData(flatInput, flatField), name, new ParserPointer(pos, size));
        }
        return getBean();
    }
    private Object parseField(String rawValue, FlatField flatField) throws ParserArgumentException {
        FieldParser<?> fieldParser = fieldParserFactory.getFieldParser(flatField.getFlatType());
        return fieldParser.parse(rawValue, flatField);
    }
    private void handleSimpleField(FlatData data, String name, ParserPointer pointer) throws Exception {
        Object value = parseField(data.parseField(pointer), data.getField());
        setValue(name, value);
    }
    private void handleLengthField(FlatData data, String name, ParserPointer pointer) throws Exception {
        String rawValue = data.parseField(pointer);
        int intValue = (Integer) parseField(rawValue, data.getField());
        quantityOfChunksOfMultipleFields.put(name, intValue);
    }
    private int handleMultipleField(FlatData data, String name, ParserPointer pointer) throws Exception {
        Integer quantityOfChunks = quantityOfChunksOfMultipleFields.get(name);
        String flatList = data.getFlatInput().substring(pointer.getPos(), pointer.getPos() + quantityOfChunks * pointer.getSize());
        if (!flatList.isEmpty()) {
            Iterable<String> chunks = Splitter.fixedLength(pointer.getSize()).split(flatList);
            for (String chunk : chunks) {
                Object value = parseField(chunk, data.getField());
                addInCollection(data.getField().getName(), value);
            }
        }
        pointer.setPos(pointer.getPos() + quantityOfChunks * pointer.getSize());
        if (data.getField().listSizeIsFixed()) {
            int quantityOfEmptyChunks = data.getField().getListFixedSize() -  quantityOfChunks;
            if (quantityOfEmptyChunks > 0)
                pointer.setPos(pointer.getPos() + quantityOfEmptyChunks * pointer.getSize());
        }
        return pointer.getPos();
    }
    private void setFillerField(String name) {
        try {
            setValue(name, null);
        } catch (IllegalStateException ex) {
            setValue(name, 0);
        }
    }
}
