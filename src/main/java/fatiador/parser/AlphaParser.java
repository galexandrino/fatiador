package fatiador.parser;

import org.apache.commons.lang3.StringUtils;

import fatiador.FlatField;
import fatiador.ParserArgumentException;

public class AlphaParser implements FieldParser<String> {

    @Override
    public String parse(String rawValue, FlatField flatField) throws ParserArgumentException {
        return StringUtils.stripEnd(rawValue, " ");
    }

}
