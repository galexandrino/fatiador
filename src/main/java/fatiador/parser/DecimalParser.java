package fatiador.parser;

import org.apache.commons.lang3.StringUtils;

import fatiador.FlatField;
import fatiador.ParserArgumentException;

public class DecimalParser implements FieldParser<Double> {

    private static final String ERROR_MESSAGE = "Field [%s] not parsable. Value [%s] not convertible to Double.";

    @Override
    public Double parse(String rawValue, FlatField flatField) throws ParserArgumentException {
    	
    	if (StringUtils.isBlank(rawValue)) {
    		return 0.0;
    	}
    	
    	double doubleValue;
        try {
            doubleValue = Double.parseDouble(rawValue);
        } catch (NumberFormatException e) {
            String msg = String.format(ERROR_MESSAGE, flatField.getName(), rawValue);
            throw new ParserArgumentException(msg, e);
        }
        int decimalDigitsSize = flatField.getDecimalDigitsSize();
        return doubleValue / Math.pow(10, decimalDigitsSize);
    }

}
