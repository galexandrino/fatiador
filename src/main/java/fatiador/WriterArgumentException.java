package fatiador;

public class WriterArgumentException extends Exception {

    private static final long serialVersionUID = 1L;

    public WriterArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public WriterArgumentException(String message) {
        super(message);
    }

}
