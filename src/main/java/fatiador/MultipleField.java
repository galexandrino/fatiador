package fatiador;

import fatiador.pojo.FlatFieldOptions;

public class MultipleField extends FlatField {

    public MultipleField(String name, int size) {
        super(name, size);
    }

    public MultipleField(String name, int size, FlatType flatType) {
        super(name, size, flatType);
    }

    public MultipleField(FlatFieldOptions options, FlatType flatType) {
        super(options, flatType);
    }

    public MultipleField(String name, FlatStructure structure, Class<?> beanClass) {
        super(name, structure, beanClass);
    }

    public MultipleField(FlatFieldOptions options, FlatStructure structure, Class<?> beanClass) {
        super(options, structure, beanClass);
    }
}
