package fatiador;

import fatiador.pojo.FlatFieldOptions;

public class SimpleField extends FlatField {

    public SimpleField(String name, int size) {
        super(name, size);
    }

    public SimpleField(String name, int size, FlatType flatType) {
        super(name, size, flatType);
    }

    public SimpleField(String name, String dateTimeFormat, FlatType flatType) {
        super(name, dateTimeFormat, flatType);
    }

    public SimpleField(String name, int size, int decimalDigitsSize) {
        super(name, size, decimalDigitsSize);
    }

    public SimpleField(FlatFieldOptions options, String valueTrue, String valueFalse) {
        super(options, valueTrue, valueFalse);
    }
}
