package fatiador;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Collections;

import com.google.common.base.Strings;

import fatiador.writer.FieldWriter;
import fatiador.writer.FieldWriterFactory;
/**
 * Use this class to convert objects of type T into flat strings.
 */
public class FlatWriter<T> {

    public static final String FIELD_NOT_WRITABLE_PREFIX = "I couldn't read the field ";
    private FlatStructure structure;
    private Class<T> productClass;
    private FieldWriterFactory fieldWriterFactory = new FieldWriterFactory();

    public FlatWriter(FlatStructure structure, Class<T> productClass) {
        this.structure = structure;
        this.productClass = productClass;
    }
    public String write(T bean) {
        try {
            return innerWrite(bean);
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }
    @SuppressWarnings("rawtypes")
    private String innerWrite(T bean) throws Exception {
        StringBuilder flat = new StringBuilder();
        for (FlatField flatField : structure.getFields()) {
            Field javaField = productClass.getDeclaredField(flatField.getName());
            javaField.setAccessible(true);
            if (flatField.getFiller()) {
                flat.append(handleFillerField(flatField.getSize()));
            } else if (flatField instanceof SimpleField) {
                flat.append(handleSimpleField(bean, javaField, flatField));
            } else if (flatField instanceof LengthField) {
                flat.append(handleLengthField(bean, javaField, flatField));
            } else if (flatField instanceof MultipleField) {
                flat.append(handleMultipleField(bean, javaField, flatField));
            }
        }
        return flat.toString();
    }
    private String handleFillerField(int size) {
        return String.join("", Collections.nCopies(size, " "));
    }
    private String handleSimpleField(T bean, Field javaField, FlatField flatField) throws Exception {
        Object value = javaField.get(bean);
        return writeField(value, flatField);
    }
    private String handleLengthField(T bean, Field javaField, FlatField flatField) throws Exception {
        Collection collection = (Collection) javaField.get(bean);
        collection = collection == null ?  Collections.emptyList() : collection;
        Integer value = collection.size();
        return Strings.padStart(value.toString(), flatField.getSize(), '0');
    }
    private String handleMultipleField(T bean, Field javaField, FlatField flatField) throws Exception {
        Collection collection = collectionFrom(bean, javaField);
        StringBuilder result = new StringBuilder();
        for (Object element : collection)
            result.append(writeField(element, flatField));
        return result.append(fillerForFixedSizeList(collection, flatField)).toString();
    }
    private Collection collectionFrom(Object bean, Field javaField) throws IllegalAccessException {
        Collection collection = (Collection) javaField.get(bean);
        return collection == null ? Collections.emptyList() : collection;
    }
    private String fillerForFixedSizeList(Collection collection, FlatField flatField) throws InstantiationException, IllegalAccessException {
        StringBuilder filler = new StringBuilder();
        if (flatField.getListFixedSize() > collection.size()) {
            String defaultFlatValue = flatField.defaultFlatValue();
            for (int i = collection.size(); i < flatField.getListFixedSize(); i++)
                filler.append(defaultFlatValue);
        }
        return filler.toString();
    }
    private String writeField(Object value, FlatField flatField) {
        FieldWriter fieldWriter = fieldWriterFactory.getFieldWriter(flatField.getFlatType());
        try {
            return fieldWriter.write(value, flatField);
        } catch (WriterArgumentException e) {
            throw new IllegalArgumentException(e.getMessage(), e.getCause());
        }
    }
}
