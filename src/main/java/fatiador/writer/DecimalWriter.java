package fatiador.writer;

import java.math.BigDecimal;

import com.google.common.base.Strings;

import fatiador.FlatField;
import fatiador.WriterArgumentException;

public class DecimalWriter implements FieldWriter {

    @Override
    public String write(Object value, FlatField flatField) throws WriterArgumentException {

        int decimalDigitsSize = flatField.getDecimalDigitsSize();
        Double doubleValue = value != null ? (Double) value : 0;

        // avoids scientific notation
        String plainString = BigDecimal.valueOf(doubleValue).toPlainString();
        
        String[] tokens = plainString.split("\\.");
        String integerPart = tokens[0];
        String decimalPart = tokens.length > 1 ? tokens[1] : "";
        
        int size = flatField.getSize();
        integerPart = Strings.padStart(integerPart, size - decimalDigitsSize, '0');
        decimalPart = Strings.padEnd(decimalPart, decimalDigitsSize, '0');
        decimalPart = decimalPart.substring(0, decimalDigitsSize);
        String flatNumber = integerPart + decimalPart;
        if (flatNumber.length() > size) {
            String msg = String.format("Number [%s] too long for field [%s] of size [%d].", doubleValue,
                    flatField.getName(), size);
            throw new WriterArgumentException(msg);
        }
        return flatNumber;
    }

}
