package fatiador.writer;

import com.google.common.base.Strings;

import fatiador.FlatField;

public class AlphaWriter implements FieldWriter {

    @Override
    public String write(Object value, FlatField flatField) {
        int size = flatField.getSize();
        String text = (String) value;
        if (text != null && text.length() > size) {
            return text.substring(0, size);
        } else {
            return Strings.padEnd(Strings.nullToEmpty(text), size, ' ');
        }
    }

}
