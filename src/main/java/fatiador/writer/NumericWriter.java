package fatiador.writer;

import com.google.common.base.Strings;
import fatiador.FlatField;

public class NumericWriter implements FieldWriter {

    @Override
    public String write(Object value, FlatField flatField) {
        int size = flatField.getSize();
        return Strings.padStart(Strings.nullToEmpty((String) value).trim(), size, '0');
    }

}
