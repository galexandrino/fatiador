package fatiador;

public class LengthField extends FlatField {

    public LengthField(String multipleFieldName, int size) {
        super(multipleFieldName, size, FlatType.INTEGER);
    }

}
