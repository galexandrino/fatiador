package fatiador;

import java.util.ArrayList;
import java.util.List;

import fatiador.pojo.FlatFieldOptions;
/**
 * Use this class to define the structure of your flat string. Think of a
 * FlatStructure as a Cobol copybook ;)
 */
public class FlatStructure {
    private String name;
    private List<FlatField> fields = new ArrayList<>();
    public FlatStructure() { }
    public FlatStructure(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public List<FlatField> getFields() {
        return fields;
    }
    public void setFillerField(String name) {
        fields.forEach(x -> {
            if (x.getName().equals(name))
                x.setFiller();
        });
    }
    public void addSimpleAlphaField(String name, int size) {
        fields.add(new SimpleField(name, size));
    }
    public void addSimpleNumericField(String name, int size) { fields.add(new SimpleField(name, size, FlatType.NUMERIC));}
    public void addSimpleIntegerField(String name, int size) { fields.add(new SimpleField(name, size, FlatType.INTEGER));}
    public void addSimpleBigIntegerField(String name, int size) { fields.add(new SimpleField(name, size, FlatType.BIG_INTEGER));}
    public void addSimpleDecimalField(String name, int size, int decimalDigitsSize) { fields.add(new SimpleField(name, size, decimalDigitsSize));}
    public void addMultipleAlphaField(String name, int size) {
        fields.add(new MultipleField(name, size));
    }
    public void addMultipleAlphaField(FlatFieldOptions options) { fields.add(new MultipleField(options, FlatType.ALPHA));}
    public void addMultipleIntegerField(String name, int size) { fields.add(new MultipleField(name, size, FlatType.INTEGER));}
    public void addMultipleIntegerField(FlatFieldOptions options) { fields.add(new MultipleField(options, FlatType.INTEGER));}
    public void addLengthField(String multipleFieldName, int size) { fields.add(new LengthField(multipleFieldName, size)); }
    public void addMultipleStructuredField(String name, FlatStructure structure, Class<?> beanClass) { fields.add(new MultipleField(name, structure, beanClass));}
    public void addField(FlatField field) {
        fields.add(field);
    }
    public int length() {
        if (structureHasAnyMultipleFieldWithoutFixedSize())
            throw new IllegalStateException("It's not possible to calculate the length of a structure with a field of multiple values");
        return fields.stream().mapToInt(f -> sizeOf(f)).sum();
    }
    private int sizeOf(FlatField f) { return f instanceof MultipleField ? f.getSize() * f.getListFixedSize() : f.getSize(); }
    private boolean structureHasAnyMultipleFieldWithoutFixedSize() { return fields.stream().anyMatch(f -> f instanceof MultipleField && f.getListFixedSize() == 0);}
    /**
     * @param name
     *            of the field
     * @param dateFormat
     *            acording to
     *            https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html.
     */
    public void addSimpleDateField(String name, String dateFormat) { fields.add(new SimpleField(name, dateFormat, FlatType.DATE));}
    /**
     * @param name
     *            of the field
     * @param timeFormat
     *            acording to
     *            https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html.
     *            One 'S' at the end means 'fraction of second'. Example: a value of '0340568'
     *            with a pattern of 'HHmmssS' means 3h 40min 56.8 s. Other uses of 'S' are not supported.
     */
    public void addSimpleTimeField(String name, String timeFormat) { fields.add(new SimpleField(name, timeFormat, FlatType.TIME));}
    /**
     * @param name
     *            of the field
     * @param dateTimeFormat
     *            acording to
     *            https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html.
     *            One 'S' at the end means 'fraction of second'. Example: a
     *            value of '0340568' with a pattern of 'HHmmssS' means 3h 40min
     *            56.8 s. Other uses of 'S' are not supported.
     *
     */
    public void addSimpleDateTimeField(String name, String dateTimeFormat) { fields.add(new SimpleField(name, dateTimeFormat, FlatType.DATE_TIME));}
    public void addBooleanField(String name, String flatTrueValue, String flatFalseValue) {
        int size = Math.max(flatTrueValue.length(), flatFalseValue.length());
        FlatFieldOptions options = new FlatFieldOptions(name, null, size);
        fields.add(new SimpleField(options, flatTrueValue, flatFalseValue));
    }
    public void addMultipleStructuredField(FlatFieldOptions options, FlatStructure structure, Class<?> beanClass) { fields.add(new MultipleField(options, structure, beanClass)); }
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("FlatStructure [fields=");
        builder.append(fields);
        builder.append("]");
        return builder.toString();
    }
}