package fatiador;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.LocalDate;
import java.time.LocalTime;

public enum FlatType {

	ALPHA(String.class, false),
	NUMERIC(String.class, true),
	INTEGER(Integer.class, true),
	BIG_INTEGER(BigInteger.class, true),
	DECIMAL(Double.class, true),
	BOOLEAN(Boolean.class, false),
	DATE(LocalDate.class, true),
	TIME(LocalTime.class, true),
	DATE_TIME(LocalDateTime.class, true),
	STRUCTURED(null, false);

	private Class<?> beanClass;
    private boolean composedOfNumbers;
	
	private FlatType(Class<?> beanClass, boolean composedOfNumbers) {
		this.beanClass = beanClass;
        this.composedOfNumbers = composedOfNumbers;
	}
	
	public Class<?> beanClass() {
		return beanClass;
	}
	
	public boolean composedOfNumbers() {
	    return composedOfNumbers;
	}
}
