package fatiador;

import java.lang.reflect.Field;
import java.util.Collection;

import fatiador.parser.AbstractFlatParser;

/**
 * Parses flat string to Java object of type T.
 *
 */
public class FlatParser<T> extends AbstractFlatParser<T> {

    private static final String PROBLEMS_IN_ACCESSING_FIELD = "Problems in accessing field ";

    private T bean;
    private Class<T> productClass;

    public FlatParser(FlatStructure structure, Class<T> productClass) {
        super(structure);
        this.productClass = productClass;
    }

    @Override
    public void setValue(String fieldName, Object value) {
        try {
            javaField(fieldName).set(bean, value);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalStateException(PROBLEMS_IN_ACCESSING_FIELD + fieldName, e);
        }
    }

    private Field javaField(String fieldName) {
        Field javaField;
        try {
            javaField = productClass.getDeclaredField(fieldName);
        } catch (NoSuchFieldException | SecurityException e) {
            throw new IllegalStateException(PROBLEMS_IN_ACCESSING_FIELD + fieldName, e);
        }
        javaField.setAccessible(true);
        return javaField;
    }

    @Override
    public T getBean() {
        return bean;
    }

    @Override
    public void addInCollection(String fieldName, Object value) throws ParserArgumentException {
        Collection<Object> collection;
        try {
            collection = (Collection<Object>) javaField(fieldName).get(bean);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalStateException(PROBLEMS_IN_ACCESSING_FIELD + fieldName, e);
        }
        if (collection == null) {
            throw new ParserArgumentException("Collection '" + fieldName + "' is null; you must initialize it.");
        }
        collection.add(value);
    }

    @Override
    public void initializeBean() {
        try {
            bean = productClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new IllegalStateException("Problems in instantiating object of type " + productClass.getName(), e);
        }

    }

}
