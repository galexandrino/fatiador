package fatiador.pojo;

public class FlatFieldOptions {
    private int listFixedSize;
    private String name;
    private int size;

    public FlatFieldOptions(String name, Integer listFixedSize, Integer size) {
        this.listFixedSize = listFixedSize == null ? -1 : listFixedSize;
        this.name = name;
        this.size = size == null ? -1 : size;
    }

    public String toString() {
        StringBuilder bd = new StringBuilder();
        bd.append("Name: ").append(name).append("\nSize:").append(size).append("\nListFixed: ").append(listFixedSize);
        return bd.toString();
    }

    public Boolean isSized() {
        return size != -1;
    }

    public Boolean isListFixedSize() {
        return listFixedSize != -1;
    }

    public int getListFixedSize() {
        return listFixedSize;
    }

    public void setListFixedSize(int listFixedSize) {
        this.listFixedSize = listFixedSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

}
