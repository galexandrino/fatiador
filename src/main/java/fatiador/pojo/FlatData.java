package fatiador.pojo;

import fatiador.FlatField;

public class FlatData {
    private String flatInput;
    private FlatField field;

    public FlatData(String flatInput, FlatField field) {
        this.flatInput = flatInput;
        this.field = field;
    }

    public String ToString() {
        return "Input: " + flatInput + "Feeds: " + field;
    }

    public String parseField(ParserPointer pointer) {
        return flatInput.substring(pointer.getPos(), pointer.getChunk());
    }

    public String getFlatInput() {
        return flatInput;
    }

    public void setFlatInput(String flatInput) {
        this.flatInput = flatInput;
    }

    public FlatField getField() {
        return field;
    }

    public void setField(FlatField field) {
        this.field = field;
    }

}
