package fatiador.pojo;

public class ParserPointer {
    private int pos;
    private int size;

    public ParserPointer(int pos, int size) {
        this.pos = pos;
        this.size = size;
    }

    public Boolean isFinished() {
        return pos == size;
    }

    public int getChunk() {
        return pos + size;
    }

    public int getPos() {
        return pos;
    }
    public void setPos(int pos) {
        this.pos = pos;
    }

    public int getSize() {
        return size;
    }
    public void setSize(int size) {
        this.size = size;
    }
}
