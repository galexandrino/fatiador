# Change log

v 1.4.0: initial public version.

v 1.5.0: parse/write of boolean values.

v 1.6.0: when parse fails, exception shows the "partial debug" to help in problem diagnostic.

v 1.7.0: writes flat string for multiple structured field; it's also possible to define a fixed length for the generated flat list.

v 1.8.0: writes and parse LocalDateTime fields. 

v 1.9.0: MapFlatParser converts flat strings to Java maps.

v 1.10.0 Writes lists with fixed size, not only for structured type.

v 1.11.0 parse/write of BigInteger values and Numeric values (for long strings composed by numbers only). 

v 1.11.1 When writing flat strings, truncates long texts and throws exception when trying to write long numbers.

v 1.12.0 MapFlatParser respects the field order in the flat structure.

v 1.13.0 Now it's possible to name a flat structure. Useful for testing purposes.

v 1.14.0 Define new field for structure using already existing field object. 

v 1.14.1 Fixes boolean writer. The previous version was writing true values in the place of false values and vice-versa.

v 1.14.2 Fixes bug that prevented using the MapFlatParser for date fields when flat value was filled with zeros.

v 1.14.3 Empty flat strings are coerced to 0 or false when parsing them. 

v 1.14.4 Partial debug shows flat input on parse error.

v 1.15.0 Parse multiple fields with fixed list size

v 1.16.0 When parsing alpha fields, only final spaces are trimmed, not the initial ones.

v 1.17.0 Supports flatField.size() for multiple fields with fixed list size.

v 1.17.1 When parsing a multiple field, the corresponding collection must be initialized by the user. If it is not initialized, now the user receives a well crafted exception rather than a null pointer exception.

v 1.17.2 Fixes bug: too long doubles were not properly written to the flat string. In this case, too long means long enough so sysout prints it in scientific notation. Ex: 12345678.9 is printed as 1.23456789E7.

v 1.18.0 Renames argument name of LengthField to be more clear.
